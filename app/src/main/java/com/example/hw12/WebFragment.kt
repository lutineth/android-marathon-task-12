package com.example.hw12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView

class WebFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewWeb = inflater.inflate(R.layout.fragment_web, container, false)
        viewWeb.findViewById<BottomNavigationView>(R.id.bottomMenu).selectedItemId = R.id.web
        viewWeb.findViewById<BottomNavigationView>(R.id.bottomMenu).setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.exit -> {
                    Navigation.findNavController(viewWeb).navigate(R.id.loginFragmentNW)
                }
                R.id.home -> {
                    Navigation.findNavController(viewWeb).navigate(R.id.homeFragmentNW)
                }
                R.id.web -> {}
            }
            true
        }
        return viewWeb
    }

    companion object {
        @JvmStatic
        fun newInstance() = WebFragment()
    }
}
