package com.example.hw12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.widget.TextView
import android.view.*
import androidx.core.view.isVisible
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationMenuView

class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewLogin =  inflater.inflate(R.layout.fragment_login, container, false)
        viewLogin.findViewById<BottomNavigationMenuView>(R.id.bottomMenu).isVisible = false;
        viewLogin.findViewById<TextView>(R.id.tvLogin).setOnClickListener{
            Navigation.findNavController(viewLogin).navigate(R.id.registrationFragmentNL)
        }
        viewLogin.findViewById<TextView>(R.id.bLogin).setOnClickListener{
            Navigation.findNavController(viewLogin).navigate(R.id.homeFragmentNL)
        }
        return viewLogin
    }

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}
