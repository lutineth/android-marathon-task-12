package com.example.hw12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewHome = inflater.inflate(R.layout.fragment_home, container, false)
        viewHome.findViewById<BottomNavigationView>(R.id.bottomMenu).selectedItemId = R.id.home
        viewHome.findViewById<BottomNavigationView>(R.id.bottomMenu).setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.exit -> {
                    Navigation.findNavController(viewHome).navigate(R.id.loginFragmentNH)
                }
                R.id.home -> {}
                R.id.web -> {
                    Navigation.findNavController(viewHome).navigate(R.id.webFragmentNH)
                }
            }
            true
        }
        return viewHome
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}
