package com.example.hw12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import android.widget.TextView
import androidx.navigation.Navigation

class RegistrationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewReg = inflater.inflate(R.layout.fragment_registration, container, false)
        viewReg.findViewById<TextView>(R.id.tvRegistration).setOnClickListener {
            Navigation.findNavController(viewReg).navigate(R.id.loginFragmentNR)
        }
        return viewReg
    }

    companion object {
        @JvmStatic
        fun newInstance() = RegistrationFragment()
    }
}
